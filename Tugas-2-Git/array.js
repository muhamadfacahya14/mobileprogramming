//1. Range
function range(startNum, finishNum){
	var ar =[];
	if(startNum==null || finishNum==null){
		return "-1";
	}
	else{
	   if(startNum<finishNum){
		   for(var i = startNum; i <= finishNum; i++) {
			   ar.push(i);
		   }
	   }else{
		   for(var i = startNum; i >= finishNum; i--) {
			   ar.push(i);
		   }
	   }
	   return ar;
	}
} 

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1
console.log('')

//2. Range with Step
function rangeWithStep(startNum, finishNum, step) {
	var ar =[];
	if(startNum==null || finishNum==null){
		return "-1";
	}
	else{
	   if(startNum<finishNum){
		   for(var i = startNum; i <= finishNum; i+=step) {
			   ar.push(i);
		   }
	   }else{
		   for(var i = startNum; i >= finishNum; i-=step) {
			   ar.push(i);
		   }
	   }
	   return ar;
	}
} 

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log('')

//3 Sum of Range
 function sum(startNum, finishNum, step) {
	var ar = 0;
	if(startNum == null && finishNum == null){
		return 0;
	}
	else if(startNum==null){
		return finishNum;
	}
	else if(finishNum==null){
		return startNum;
	}
	else{
		if(step == null){
			step = 1;
		}
		if(startNum<finishNum){
		   for(var i = startNum; i <= finishNum; i+=step) {
			   ar += i;
		   }
	   }
	   else{
		   for(var i = startNum; i >= finishNum; i-=step) {
			   ar += i;
			   }
		   }
	   return ar;
	}
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log('')

//4. Array Multidimensi
function dataHandling(bioArr){
  for(var i = 0; i < bioArr.length; i++){
    console.log("Nomor ID: " + bioArr[i][0]);
    console.log("Nama Lengkap: " + bioArr[i][1]);
    console.log("TTL: " + bioArr[i][2] + " " + bioArr[i][3]);
    console.log("Hobi: " + bioArr[i][4]);
    console.log("");
  }
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

console.log(dataHandling(input));
console.log('')

//5. Balik Kata
function balikKata(kata) {
  var kataBaru = '';         
  for (var i = 0; i <= kata.length -1; i++){  
      kataBaru = kata[i] + kataBaru;      
  }
 return kataBaru;
  
}
  
  console.log(balikKata('Kasur Rusak')); // kasuR rusaK
  console.log(balikKata('Informatika')); // akitamrofnI
  console.log(balikKata('Haji Ijah')); // hajI ijaH
  console.log(balikKata('racecar')); // racecar
  console.log(balikKata('I am Humanikers')); // srekinamuH ma I
  console.log('')

  //6. Metode Array
  function dataHandling2(bioArr){
    const newBioArr = bioArr;
    newBioArr.splice(1,2,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    newBioArr.splice(4,1, "Pria", "SMA Internasional Metro");
    
    const name = newBioArr[1];
    const month = newBioArr[3].split("/")[1]
    const dateArr = newBioArr[3].split("/")
    dateArr.sort(function(a, b) {
      return b-a
    });
    const dateString = newBioArr[3].split("/").join("-")
    
    switch(parseInt(month)){
      case 01:
        monthName = 'Januari';
        break;
      case 02:
        monthName = 'Febuari';
        break;
      case 03:
        monthName = 'Maret';
        break;
      case 04:
        monthName = 'April';
        break;
      case 05:
        monthName = 'Mei';
        break;
      case 06:
        monthName = 'Juni';
        break;
      case 07:
        monthName = 'Juli';
        break;
      case 08:
        monthName = 'Agustus';
        break;
      case 09:
        monthName = 'September';
        break;
      case 10:
        monthName = 'Oktober';
        break;
      case 11:
        monthName = 'November';
        break;
      case 12:
        monthName = 'Desember';
        break;
      default:
        monthName = "Tidak ada bulan yang lebih dari 12";
        break;
    };
    
    console.log(newBioArr);
    console.log(monthName);
    console.log(dateArr);
    console.log(dateString);
    console.log(name);
  }
  
  
  var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
  dataHandling2(input);

//1 (Array to Object)
function arrayToObject(key){
    var obj = {}
    if (key == 0){
        console.log('""')
    }else {
        for(var i = 0; i < key.length; i++){
            console.log((i + 1) + ". " + key[i][0] + " " + key[i][1])
            obj.firstName = key[i][0];
            obj.lastName = key[i][1];
            obj.gander = key[i][2];
            if (!key[i][3] || key[i][3] > 2021){
                obj.age = "Invalid Birth Year"
            }else {
                obj.age = 2021 - key[i][3];
            }
            console.log(obj)
        }
    }
}
var poeple = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(poeple)
console.log("")
var poeple2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(poeple2)
console.log('')
arrayToObject([])
console.log('')

//2 Shopping Time
function shoppingTime(memberId, money) {
    var kembalian = 0
    var sale = [['Sepatu Stacattu', 1500000],
                ['Baju Zoro', 500000],
                ['Baju H&N', 250000],
                ['Sweater Unikolah', 175000],
                ['Casing Handphone', 50000]]
    var result = {
        memberId: '',
        money: 0,
        listPurchased: [],
        changeMoney: 0
    }
    if (!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    if (money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }
    result.memberId = memberId;
    result.money = money;
    for(var i = sale.length -1; i >= 0; i--){
        if (money > sale[i][1]){
            result.listPurchased.push(sale[i][0])
            kembalian = money - sale [i][1];
        }
        else {
            result.changeMoney = kembalian
        }
    }
    return result
}

console.log(shoppingTime('180RzKrnWn08', 2475000));
console.log('')
console.log(shoppingTime('82Ku8ma742', 170000));
console.log('')
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X berlaku untuk member saja
console.log('')
console.log(shoppingTime('234JdhweRxa53', 15000));//Mohon maaf, uang tidak cukup
console.log('')
console.log(shoppingTime());//Mohon maaf, toko X hanya berlaku untuk member sajaq
console.log('')

//3(Naik Angkot)
function naikAngkot(listPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E','F'];
    var output = [];
    var bayar = 0
    var naik = 0;
    var turun = 0;
    for(var i = 0; i < listPenumpang.length; i++){
        for (var j = 0; j < rute.length; j++){
            if (listPenumpang[i][1] === rute[j]){
                niak = j
            }else if (listPenumpang[i][2] === rute[j]){
                turun =j
            }
        }
        bayar = (turun-naik)*2000
        output.push({
            penumpang : listPenumpang[i][0],
            naikDari : listPenumpang[i][1],
            tujuan : listPenumpang[i][2],
            bayar : bayar
        })
    }
    return output
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log('')
console.log(naikAngkot([]))
